use ::serde::{Deserialize, Serialize};
use axum::{
    extract::FromRequestParts,
    http::{request::Parts, StatusCode},
};

use crate::response::CustomError;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Sort {
    #[serde(rename = "key")]
    pub key: String,
    #[serde(rename = "order")]
    pub order: String,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct PaginatedItems<T> {
    pub items: Vec<T>,
    #[serde(rename = "totalItems")]
    pub total_items: u64,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct NameOrDescriptionSearchFilters {
    #[serde(rename = "nameOrDescription")]
    pub name_or_description: Option<String>,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct PaginateSortGroupBy {
    #[serde(rename = "page")]
    pub page: u64,
    #[serde(rename = "itemsPerPage")]
    pub items_per_page: i64,
    #[serde(rename = "sortBy")]
    pub sort_by: Option<Vec<Sort>>,
    #[serde(rename = "groupBy")]
    pub group_by: Option<Vec<String>>,
}

impl PaginateSortGroupBy {
    pub fn new() -> PaginateSortGroupBy {
        PaginateSortGroupBy {
            page: 0,
            items_per_page: 10,
            sort_by: None,
            group_by: None,
        }
    }
}

impl<S> FromRequestParts<S> for PaginateSortGroupBy
where
    S: Send + Sync,
{
    type Rejection = CustomError;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        if let Some(query) = parts.uri.query() {
            dbg!(query);
            let query = String::from(query);
            let params_blocks: Vec<&str> = query.split("&").collect();
            let mut paginate_sort_group_by = PaginateSortGroupBy::new();
            for pb in params_blocks {
                let param_value: Vec<&str> = pb.split('=').collect();
                if param_value.len() != 2 {
                    return Err(CustomError::new(
                        StatusCode::BAD_REQUEST,
                        "Error parsing params, only 1 '=' char is allowed per param",
                    ));
                }

                let (param, value) = (param_value[0], param_value[1]);
                let mut param_name = param;
                if let Some(mut opened_bracket_index) = param.find('[') {
                    param_name = &param[..opened_bracket_index];
                    if let Some(mut closed_bracket_index) = param[opened_bracket_index..].find(']')
                    {
                        closed_bracket_index += opened_bracket_index;
                        opened_bracket_index += 1;
                        let array_index_str = &param[opened_bracket_index..closed_bracket_index];
                        let array_index = array_index_str.parse::<usize>()?;
                        if let Some(mut opened_bracket_index) =
                            param[closed_bracket_index..].find('[')
                        {
                            opened_bracket_index += closed_bracket_index;
                            opened_bracket_index += 1;
                            if let Some(mut closed_bracket_index) =
                                param[opened_bracket_index..].find(']')
                            {
                                closed_bracket_index += opened_bracket_index;
                                let field_name = &param[opened_bracket_index..closed_bracket_index];
                                match param_name {
                                    "sortBy" => {
                                        paginate_sort_group_by.sort_by = if let Some(mut sort_by) =
                                            paginate_sort_group_by.sort_by
                                        {
                                            if array_index < sort_by.len() {
                                                match field_name {
                                                    "key" => {
                                                        sort_by[array_index].key =
                                                            value.to_string();
                                                    }
                                                    "order" => {
                                                        sort_by[array_index].order =
                                                            value.to_string();
                                                    }
                                                    _ => {}
                                                }
                                            } else {
                                                let mut field_value = Sort {
                                                    key: String::new(),
                                                    order: String::new(),
                                                };
                                                match field_name {
                                                    "key" => {
                                                        field_value.key = value.to_string();
                                                    }
                                                    "order" => {
                                                        field_value.order = value.to_string();
                                                    }
                                                    _ => {}
                                                }
                                                sort_by.push(field_value);
                                            }

                                            Some(sort_by)
                                        } else {
                                            let mut field_value = Sort {
                                                key: String::new(),
                                                order: String::new(),
                                            };
                                            match field_name {
                                                "key" => {
                                                    field_value.key = value.to_string();
                                                }
                                                "order" => {
                                                    field_value.order = value.to_string();
                                                }
                                                _ => {}
                                            }

                                            Some(vec![field_value])
                                        }
                                    }
                                    "groupBy" => {}
                                    _ => {}
                                }
                            }
                        }
                    }
                } else {
                    match param_name {
                        "page" => {
                            paginate_sort_group_by.page = value.parse::<u64>()?;
                        }
                        "itemsPerPage" => {
                            paginate_sort_group_by.items_per_page = value.parse::<i64>()?;
                        }
                        _ => {}
                    }
                }
            }
            return Ok(paginate_sort_group_by);
        }

        Err(CustomError::new(
            StatusCode::BAD_REQUEST,
            "Unknown error occurred parsing uri",
        ))
    }
}
