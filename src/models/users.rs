use chrono::NaiveDateTime;
use entity::user::Model;
use serde::{Deserialize, Serialize};

use super::roles::Role;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Login {
    pub email: String,
    pub password: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct CreatedUser {
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub role: Option<Role>,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct ModifiedUser {
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub email: Option<String>,
    pub role: Option<Role>,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct User {
    pub id: i32,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub created_at: NaiveDateTime,
    pub updated_at: Option<NaiveDateTime>,
    pub role: Option<Role>,
}

impl From<&Model> for User {
    fn from(model: &Model) -> User {
        User {
            id: model.id,
            first_name: model.first_name.clone(),
            last_name: model.last_name.clone(),
            email: model.email.clone(),
            created_at: model.created_at,
            updated_at: model.updated_at,
            role: None,
        }
    }
}

impl
    From<&(
        entity::user::Model,
        (entity::role::Model, Vec<entity::permission::Model>),
    )> for User
{
    fn from(
        (user, role_permissions): &(
            entity::user::Model,
            (entity::role::Model, Vec<entity::permission::Model>),
        ),
    ) -> User {
        User {
            id: user.id,
            first_name: user.first_name.clone(),
            last_name: user.last_name.clone(),
            email: user.email.clone(),
            created_at: user.created_at,
            updated_at: user.updated_at,
            role: Some(Role::from(role_permissions)),
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct UserSearchFilters {
    pub name: Option<String>,
    pub email: Option<String>,
    pub from: Option<String>,
    pub to: Option<String>,
}
