use entity::custom_value::Model;
use sea_orm::FromQueryResult;
use serde::{Deserialize, Serialize};
use serde_json::Value as Json;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Item {
    pub name: String,
    pub description: Option<String>,
}

#[derive(Clone, Debug, Default, FromQueryResult, Serialize, Deserialize)]
pub struct BaseCustomValue {
    pub code: String,
    pub description: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct CreatedCustomValue {
    pub code: String,
    pub description: String,
    pub schema: Json,
    pub items: Json,
    #[serde(rename = "isActive")]
    pub is_active: bool,
}


#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct CustomValue {
    pub id: i32,
    pub code: String,
    pub description: String,
    pub schema: Json,
    pub items: Json,
    #[serde(rename = "isActive")]
    pub is_active: bool,
}

impl From<&Model> for CustomValue {
    fn from(model: &Model) -> CustomValue {
        CustomValue {
            id: model.id,
            code: model.code.clone(),
            description: model.description.clone(),
            schema: model.schema.clone(),
            items: model.items.clone(),
            is_active: model.is_active,
        }
    }
}