use entity::role::Model;
use serde::{Deserialize, Serialize};

use super::permissions::Permission;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct CreatedRole {
    pub name: String,
    pub description: Option<String>,
    pub permissions: Vec<i32>,
}

impl From<&(entity::role::Model, Vec<entity::permission::Model>)> for CreatedRole {
    fn from(
        (role, permissions): &(entity::role::Model, Vec<entity::permission::Model>),
    ) -> CreatedRole {
        CreatedRole {
            name: role.name.clone(),
            description: role.description.clone(),
            permissions: permissions.iter().map(|item| item.id).collect(),
        }
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct UpdatedRole {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub permissions: Vec<i32>,
}

impl From<&(entity::role::Model, Vec<entity::permission::Model>)> for UpdatedRole {
    fn from(
        (role, permissions): &(entity::role::Model, Vec<entity::permission::Model>),
    ) -> UpdatedRole {
        UpdatedRole {
            id: role.id,
            name: role.name.clone(),
            description: role.description.clone(),
            permissions: permissions.iter().map(|item| item.id).collect(),
        }
    }
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Role {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
    pub permissions: Vec<Permission>,
}

impl From<&Model> for Role {
    fn from(model: &Model) -> Role {
        Role {
            id: model.id,
            name: model.name.clone(),
            description: model.description.clone(),
            permissions: Vec::new(),
        }
    }
}

impl From<&(entity::role::Model, Vec<entity::permission::Model>)> for Role {
    fn from((role, permissions): &(entity::role::Model, Vec<entity::permission::Model>)) -> Role {
        Role {
            id: role.id,
            name: role.name.clone(),
            description: role.description.clone(),
            permissions: permissions.iter().map(|item| item.into()).collect(),
        }
    }
}
