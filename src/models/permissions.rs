use entity::permission::Model;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct CreatedPermission {
    pub name: String,
    pub description: Option<String>,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct Permission {
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
}

impl From<&Model> for Permission {
    fn from(model: &Model) -> Permission {
        Permission {
            id: model.id,
            name: model.name.clone(),
            description: model.description.clone(),
        }
    }
}
