use crate::{
    models::{
        custom_values::{BaseCustomValue, CreatedCustomValue, CustomValue},
        filter_pagination_sorting::{
            PaginateSortGroupBy, PaginatedItems,
        },
    },
    response::CustomError,
    services::{self, jwt_session::authorize_request_with_permission},
    AppState,
};
use axum::{
    extract::*,
    middleware::{self, Next},
    response::Json,
    routing::{delete, get, patch, post},
    Router,
};
use serde_json::Value as JsonValue;
use std::sync::Arc;

async fn get_custom_values(
    pagination: PaginateSortGroupBy,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<PaginatedItems<CustomValue>>, CustomError> {
    let paginated_custom_values =
        services::custom_values::get_paginated_custom_values(&app_state.db_connection, &pagination)
            .await?;
    Ok(Json(paginated_custom_values))
}

async fn get_all_custom_values(
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<Vec<BaseCustomValue>>, CustomError> {
    let custom_values =
        services::custom_values::get_all_custom_values(&app_state.db_connection).await?;
    Ok(Json(custom_values))
}

async fn get_custom_value_by_code(
    Path(code): Path<String>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<CustomValue>, CustomError> {
    let custom_value =
        services::custom_values::get_custom_value_by_code(&app_state.db_connection, code).await?;
    Ok(Json(custom_value))
}

async fn get_schema_by_code(
    Path(code): Path<String>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<JsonValue>, CustomError> {
    let custom_value_schema =
        services::custom_values::get_schema_by_code(&app_state.db_connection, code).await?;
    Ok(Json(custom_value_schema))
}

async fn get_items_by_code(
    Path(code): Path<String>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<JsonValue>, CustomError> {
    let custom_value_items =
        services::custom_values::get_items_by_code(&app_state.db_connection, code).await?;
    Ok(Json(custom_value_items))
}

async fn create_custom_value(
    State(app_state): State<Arc<AppState>>,
    Json(item): Json<CreatedCustomValue>,
) -> Result<Json<CustomValue>, CustomError> {
    let custom_value =
        services::custom_values::create_custom_value(&app_state.db_connection, item).await?;
    Ok(Json(custom_value))
}

async fn update_custom_value(
    State(app_state): State<Arc<AppState>>,
    Path(code): Path<String>,
    Json(item): Json<CustomValue>,
) -> Result<Json<CustomValue>, CustomError> {
    let custom_value =
        services::custom_values::update_custom_value(&app_state.db_connection, code, item).await?;
    Ok(Json(custom_value))
}

async fn update_schema(
    State(app_state): State<Arc<AppState>>,
    Path(code): Path<String>,
    Json(schema): Json<JsonValue>,
) -> Result<Json<CustomValue>, CustomError> {
    let custom_value =
        services::custom_values::update_schema(&app_state.db_connection, code, schema).await?;
    Ok(Json(custom_value))
}

async fn update_items(
    State(app_state): State<Arc<AppState>>,
    Path(code): Path<String>,
    Json(item): Json<JsonValue>,
) -> Result<Json<CustomValue>, CustomError> {
    let custom_value =
        services::custom_values::update_items(&app_state.db_connection, code, item).await?;
    Ok(Json(custom_value))
}

async fn delete_custom_value(
    Path(code): Path<String>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<u64>, CustomError> {
    let rows_affected =
        services::custom_values::delete_custom_value(&app_state.db_connection, code).await?;
    Ok(Json(rows_affected))
}

pub fn get_custom_values_router(app_state: &Arc<AppState>) -> Router {
    let app_state_read = app_state.clone();
    let readable_routes = Router::new()
        .route("/", get(get_custom_values))
        .route("/{code}", get(get_custom_value_by_code))
        .route("/{code}/schema", get(get_schema_by_code))
        .route("/{code}/items", get(get_items_by_code))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission(
                "CUSTOM_VALUE_READ",
                app_state_read.clone(),
                req,
                next,
            )
        }));

    let app_state_write = app_state.clone();
    let writable_routes = Router::new()
        .route("/create", post(create_custom_value))
        .route("/{code}", patch(update_custom_value))
        .route("/{code}/update-schema", patch(update_schema))
        .route("/{code}/update-items", patch(update_items))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission(
                "CUSTOM_VALUE_WRITE",
                app_state_write.clone(),
                req,
                next,
            )
        }));

    let app_state_delete = app_state.clone();
    let deletable_routes = Router::new()
        .route("/{code}", delete(delete_custom_value))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission(
                "CUSTOM_VALUE_DELETE",
                app_state_delete.clone(),
                req,
                next,
            )
        }));

    let merged_routes = Router::new()
        .route(
            "/all",
            get(get_all_custom_values).route_layer(middleware::from_fn_with_state(
                app_state.clone(),
                services::jwt_session::authorize_request,
            )),
        )
        .merge(readable_routes)
        .merge(writable_routes)
        .merge(deletable_routes);

    merged_routes.with_state(app_state.clone())
}
