use crate::{
    models::filter_pagination_sorting::PaginateSortGroupBy,
    response::{CustomError, HttpStatusCodeMessageError},
};
use anyhow::anyhow;
use axum::{http::StatusCode, response::Json, routing::get, Router};
use jsonwebtoken::errors::ErrorKind;
use rand::Rng;
use sea_orm::DbErr;
use std::env;

async fn get_random_error() -> Result<&'static str, CustomError> {
    let mut rng = rand::thread_rng();
    let error_type: u32 = rng.gen_range(1..=25);
    match error_type {
        1 => Err(CustomError::AnyhowError(anyhow!("Anyhow error"))),
        2 => Err(CustomError::VarError(env::VarError::NotPresent)),
        3 => Err(CustomError::JsonWebTokenError(
            ErrorKind::InvalidToken.into(),
        )),
        4 => Err(CustomError::JsonWebTokenError(
            ErrorKind::InvalidSignature.into(),
        )),
        5 => Err(CustomError::DbError(DbErr::RecordNotFound(
            "Record not found".to_owned(),
        ))),
        6 => Err(CustomError::DbError(DbErr::AttrNotSet(
            "Attribute not set".to_owned(),
        ))),
        7 => Err(CustomError::DbError(DbErr::Type("Type error".to_owned()))),
        8 => Err(CustomError::DbError(DbErr::Json("Json error".to_owned()))),
        9 => Err(CustomError::DbError(DbErr::RecordNotInserted)),
        10 => Err(CustomError::DbError(DbErr::RecordNotUpdated)),
        11 => Err(CustomError::DbError(DbErr::ConnectionAcquire(
            sea_orm::ConnAcquireErr::Timeout,
        ))),
        12 => Err(CustomError::DbError(DbErr::Conn(
            sea_orm::RuntimeErr::Internal("Connection failed".to_owned()),
        ))),
        13 => Err(CustomError::DbError(DbErr::Exec(
            sea_orm::RuntimeErr::Internal("Execution failed".to_owned()),
        ))),
        14 => Err(CustomError::DbError(DbErr::Query(
            sea_orm::RuntimeErr::Internal("Query failed".to_owned()),
        ))),
        15 => Err(CustomError::DbError(DbErr::ConvertFromU64(
            "Specified type cannot be converted from u64",
        ))),
        16 => Err(CustomError::DbError(DbErr::UnpackInsertId)),
        17 => Err(CustomError::DbError(DbErr::UpdateGetPrimaryKey)),
        18 => Err(CustomError::DbError(DbErr::Custom(
            "Custom error occurred".to_owned(),
        ))),
        19 => Err(CustomError::DbError(DbErr::Migration(
            "Migration error occurred".to_owned(),
        ))),
        20 => {
            let http_error_code = StatusCode::from_u16(rng.gen_range(400..500)).unwrap();
            Err(CustomError::StatusCodeMessageError(
                HttpStatusCodeMessageError {
                    status: http_error_code,
                    message: format!("Random http error with code {}", http_error_code),
                },
            ))
        }
        21 => Err(CustomError::new(StatusCode::UNAUTHORIZED, "Unauthorized")),
        //_ => Err(CustomError::new(StatusCode::FORBIDDEN, "Forbidden")),
        _ => Ok("Valid response"),
    }
}

async fn test_query_params(
    params: PaginateSortGroupBy,
) -> Result<Json<PaginateSortGroupBy>, CustomError> {
    dbg!(&params);
    Ok(Json(params))
}

pub fn get_tests_router() -> Router {
    Router::new()
        .route("/random-error", get(get_random_error))
        .route("/query-params", get(test_query_params))
}
