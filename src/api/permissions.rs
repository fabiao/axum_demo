use crate::{
    models::{
        filter_pagination_sorting::{
            NameOrDescriptionSearchFilters, PaginateSortGroupBy, PaginatedItems,
        },
        permissions::*,
        users::User,
    },
    response::CustomError,
    services::{self, jwt_session::authorize_request_with_permission},
    AppState,
};
use axum::{
    extract::*,
    middleware::{self, Next},
    response::Json,
    routing::{delete, get, patch, post},
    Router,
};
use std::sync::Arc;

async fn get_permissions(
    pagination: PaginateSortGroupBy,
    Query(filter): Query<NameOrDescriptionSearchFilters>,
    State(app_state): State<Arc<AppState>>,
    user: User,
) -> Result<Json<PaginatedItems<Permission>>, CustomError> {
    log::debug!("{:?}", user);
    let paginated_permissions = services::permissions::get_paginated_permissions(
        &app_state.db_connection,
        &filter,
        &pagination,
    )
    .await?;
    Ok(Json(paginated_permissions))
}

async fn get_all_permissions(
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<Vec<Permission>>, CustomError> {
    let permissions = services::permissions::get_all_permissions(&app_state.db_connection).await?;
    Ok(Json(permissions))
}

async fn get_permission_by_id(
    Path(id): Path<i32>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<Permission>, CustomError> {
    let permission =
        services::permissions::get_permission_by_id(&app_state.db_connection, id).await?;
    Ok(Json(permission))
}

async fn create_permission(
    State(app_state): State<Arc<AppState>>,
    Json(item): Json<CreatedPermission>,
) -> Result<Json<Permission>, CustomError> {
    let permission =
        services::permissions::create_permission(&app_state.db_connection, item).await?;
    Ok(Json(permission))
}

async fn update_permission(
    State(app_state): State<Arc<AppState>>,
    Path(id): Path<i32>,
    Json(item): Json<Permission>,
) -> Result<Json<Permission>, CustomError> {
    let permission =
        services::permissions::update_permission(&app_state.db_connection, id, item).await?;
    Ok(Json(permission))
}

async fn delete_permission(
    Path(id): Path<i32>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<u64>, CustomError> {
    let rows_affected =
        services::permissions::delete_permission(&app_state.db_connection, id).await?;
    Ok(Json(rows_affected))
}

pub fn get_permissions_router(app_state: &Arc<AppState>) -> Router {
    let app_state_read = app_state.clone();
    let readable_routes = Router::new()
        .route("/", get(get_permissions))
        .route("/{id}", get(get_permission_by_id))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission("PERMISSION_READ", app_state_read.clone(), req, next)
        }));

    let app_state_write = app_state.clone();
    let writable_routes = Router::new()
        .route("/create", post(create_permission))
        .route("/{id}", patch(update_permission))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission(
                "PERMISSION_WRITE",
                app_state_write.clone(),
                req,
                next,
            )
        }));

    let app_state_delete = app_state.clone();
    let deletable_routes = Router::new()
        .route("/{id}", delete(delete_permission))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission(
                "PERMISSION_DELETE",
                app_state_delete.clone(),
                req,
                next,
            )
        }));

    let merged_routes = Router::new()
        .route(
            "/all",
            get(get_all_permissions).route_layer(middleware::from_fn_with_state(
                app_state.clone(),
                services::jwt_session::authorize_request,
            )),
        )
        .merge(readable_routes)
        .merge(writable_routes)
        .merge(deletable_routes);

    merged_routes.with_state(app_state.clone())
}
