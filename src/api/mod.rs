pub mod custom_values;
pub mod permissions;
pub mod roles;
pub mod tests;
pub mod user_sessions;
pub mod users;

use crate::AppState;
use axum::Router;
use custom_values::get_custom_values_router;
use permissions::get_permissions_router;
use roles::get_roles_router;
use std::sync::Arc;
use tests::get_tests_router;
use user_sessions::get_user_sessions_router;
use users::get_users_router;

pub fn get_api_router(app_state: &Arc<AppState>) -> Router {
    Router::new()
        .nest("/permissions", get_permissions_router(app_state))
        .nest("/roles", get_roles_router(app_state))
        .nest("/users", get_users_router(app_state))
        .nest("/custom-values", get_custom_values_router(app_state))
        .nest("/tests", get_tests_router())
        .merge(get_user_sessions_router(app_state))
}
