use crate::{models::users::Login, response::CustomError, services, AppState};
use axum::{body::Body, extract::State, http::Request, middleware, routing::post, Json, Router};
use std::sync::Arc;

async fn login(
    State(app_state): State<Arc<AppState>>,
    Json(item): Json<Login>,
) -> Result<String, CustomError> {
    let user = services::users::get_user_by_email(&app_state.db_connection, &item.email).await?;
    let token = services::jwt_session::generate_token(
        app_state.jwt_max_age,
        app_state.jwt_secret.as_ref(),
        &user,
    )?;
    log::info!("User {} logged in", item.email);
    Ok(token)
}

async fn logout(
    State(app_state): State<Arc<AppState>>,
    mut req: Request<Body>,
) -> Result<(), CustomError> {
    let user = services::jwt_session::retrieve_session_user(
        &app_state.db_connection,
        &app_state.jwt_secret,
        &mut req,
    )
    .await?;
    log::info!("User {} logged out", user.email);
    Ok(())
}

pub fn get_user_sessions_router(app_state: &Arc<AppState>) -> Router {
    Router::new()
        .route("/login", post(login))
        .route(
            "/logout",
            post(logout).route_layer(middleware::from_fn_with_state(
                app_state.clone(),
                services::jwt_session::authorize_request,
            )),
        )
        .with_state(app_state.clone())
}
