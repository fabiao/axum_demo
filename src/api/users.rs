use std::sync::Arc;

use crate::{
    models::{
        filter_pagination_sorting::{PaginateSortGroupBy, PaginatedItems},
        users::*,
    },
    response::CustomError,
    services::{self, jwt_session::authorize_request_with_permission},
    AppState,
};
use axum::{
    body::Body,
    extract::*,
    middleware::{self, Next},
    response::Json,
    routing::{delete, get, patch, post},
    Router,
};

async fn get_users(
    pagination: PaginateSortGroupBy,
    Query(filter): Query<UserSearchFilters>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<PaginatedItems<User>>, CustomError> {
    let paginated_users =
        services::users::get_paginated_users(&app_state.db_connection, &filter, &pagination)
            .await?;
    Ok(Json(paginated_users))
}

async fn get_all_users(
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<Vec<User>>, CustomError> {
    let users = services::users::get_all_users(&app_state.db_connection).await?;
    Ok(Json(users))
}

async fn get_user_by_id(
    Path(id): Path<i32>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<User>, CustomError> {
    let user = services::users::get_user_by_id(&app_state.db_connection, id).await?;
    Ok(Json(user))
}

async fn profile(
    State(app_state): State<Arc<AppState>>,
    req: Request<Body>,
) -> Result<Json<User>, CustomError> {
    let token = services::jwt_session::extract_token(&req)?;
    let user = services::jwt_session::extract_user_info(
        &app_state.db_connection,
        &app_state.jwt_secret,
        token,
    )
    .await?;
    Ok(Json(user))
}

async fn create_user(
    State(app_state): State<Arc<AppState>>,
    Json(item): Json<CreatedUser>,
) -> Result<Json<User>, CustomError> {
    let user = services::users::create_user(&app_state.db_connection, item).await?;
    Ok(Json(user))
}

async fn update_user(
    State(app_state): State<Arc<AppState>>,
    Path(id): Path<i32>,
    Json(item): Json<ModifiedUser>,
) -> Result<Json<User>, CustomError> {
    let user = services::users::update_user(&app_state.db_connection, id, item).await?;
    Ok(Json(user))
}

async fn delete_user(
    Path(id): Path<i32>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<u64>, CustomError> {
    let rows_affected = services::users::delete_user(&app_state.db_connection, id).await?;
    Ok(Json(rows_affected))
}

pub fn get_users_router(app_state: &Arc<AppState>) -> Router {
    let app_state_read = app_state.clone();
    let readable_routes = Router::new()
        .route("/", get(get_users))
        .route("/all", get(get_all_users))
        .route("/{id}", get(get_user_by_id))
        .route("/profile", get(profile))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission("USER_READ", app_state_read.clone(), req, next)
        }));

    let app_state_write = app_state.clone();
    let writable_routes = Router::new()
        .route("/create", post(create_user))
        .route("/{id}", patch(update_user))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission("USER_WRITE", app_state_write.clone(), req, next)
        }));

    let app_state_delete = app_state.clone();
    let deletable_routes = Router::new()
        .route("/{id}", delete(delete_user))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission("USER_DELETE", app_state_delete.clone(), req, next)
        }));

    let merged_routes = Router::new()
        .merge(readable_routes)
        .merge(writable_routes)
        .merge(deletable_routes);

    merged_routes.with_state(app_state.clone())
}
