use crate::{
    models::{
        filter_pagination_sorting::{
            NameOrDescriptionSearchFilters, PaginateSortGroupBy, PaginatedItems,
        },
        roles::*,
    },
    response::CustomError,
    services::{self, jwt_session::authorize_request_with_permission},
    AppState,
};
use axum::{
    extract::*,
    middleware::{self, Next},
    response::Json,
    routing::{delete, get, patch, post},
    Router,
};
use std::sync::Arc;

async fn get_roles(
    pagination: PaginateSortGroupBy,
    Query(filter): Query<NameOrDescriptionSearchFilters>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<PaginatedItems<Role>>, CustomError> {
    let paginated_roles =
        services::roles::get_paginated_roles(&app_state.db_connection, &filter, &pagination)
            .await?;
    Ok(Json(paginated_roles))
}

async fn get_all_roles(
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<Vec<Role>>, CustomError> {
    let roles = services::roles::get_all_roles(&app_state.db_connection).await?;
    Ok(Json(roles))
}

async fn get_role_by_id(
    Path(id): Path<i32>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<UpdatedRole>, CustomError> {
    let role = services::roles::get_role_by_id(&app_state.db_connection, id).await?;
    Ok(Json(role))
}

async fn create_role(
    State(app_state): State<Arc<AppState>>,
    Json(item): Json<CreatedRole>,
) -> Result<Json<UpdatedRole>, CustomError> {
    let role = services::roles::create_role(&app_state.db_connection, &item).await?;
    Ok(Json(role))
}

async fn update_role(
    State(app_state): State<Arc<AppState>>,
    Path(id): Path<i32>,
    Json(item): Json<UpdatedRole>,
) -> Result<Json<UpdatedRole>, CustomError> {
    let role = services::roles::update_role(&app_state.db_connection, id, &item).await?;
    Ok(Json(role))
}

async fn delete_role(
    Path(id): Path<i32>,
    State(app_state): State<Arc<AppState>>,
) -> Result<Json<u64>, CustomError> {
    let rows_affected = services::roles::delete_role(&app_state.db_connection, id).await?;
    Ok(Json(rows_affected))
}

pub fn get_roles_router(app_state: &Arc<AppState>) -> Router {
    let app_state_read = app_state.clone();
    let readable_routes = Router::new()
        .route("/", get(get_roles))
        .route("/all", get(get_all_roles))
        .route("/{id}", get(get_role_by_id))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission("ROLE_READ", app_state_read.clone(), req, next)
        }));

    let app_state_write = app_state.clone();
    let writable_routes = Router::new()
        .route("/create", post(create_role))
        .route("/{id}", patch(update_role))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission("ROLE_WRITE", app_state_write.clone(), req, next)
        }));

    let app_state_delete = app_state.clone();
    let deletable_routes = Router::new()
        .route("/{id}", delete(delete_role))
        .route_layer(middleware::from_fn(move |req, next: Next| {
            authorize_request_with_permission("ROLE_DELETE", app_state_delete.clone(), req, next)
        }));

    let merged_routes = Router::new()
        .merge(readable_routes)
        .merge(writable_routes)
        .merge(deletable_routes);

    merged_routes.with_state(app_state.clone())
}
