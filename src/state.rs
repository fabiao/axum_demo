use sea_orm::DatabaseConnection;

#[derive(Debug, Clone)]
pub struct AppState {
    pub db_connection: DatabaseConnection,
    pub jwt_secret: String,
    pub _jwt_expires_in: String,
    pub jwt_max_age: i64,
}

impl Default for AppState {
    fn default() -> Self {
        Self {
            db_connection: Default::default(),
            jwt_secret: Default::default(),
            _jwt_expires_in: Default::default(),
            jwt_max_age: Default::default(),
        }
    }
}
