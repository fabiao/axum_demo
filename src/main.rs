mod api;
mod database;
mod middleware_stack;
mod models;
mod response;
mod services;
mod state;

use api::get_api_router;
use axum::error_handling::HandleErrorLayer;
use axum::http::StatusCode;
use axum::BoxError;
use axum::{routing::get, Router};
use database::get_db_connection;
use response::CustomError;
use state::AppState;
use std::sync::Arc;
use std::time::Duration;
use std::{
    env,
    net::{IpAddr, Ipv4Addr, SocketAddr},
};
use tokio::net::TcpListener;
use tower::timeout::TimeoutLayer;
use tower::ServiceBuilder;

use crate::middleware_stack::compose_middleware_stack;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::DEBUG)
        .compact()
        .init();

    env::set_var("RUST_LOG", "debug");
    dotenv::dotenv().ok();

    let host = env::var("HOST").expect("HOST is not set in .env file");
    let ip = host.parse::<Ipv4Addr>().unwrap();
    let port = env::var("PORT")
        .expect("PORT is not set in .env file")
        .parse()
        .unwrap();
    let jwt_secret = env::var("JWT_SECRET").expect("JWT_SECRET is not set in .env file");
    let jwt_expires_in =
        env::var("JWT_EXPIRED_IN").expect("JWT_EXPIRED_IN is not set in .env file");
    let jwt_max_age = env::var("JWT_MAX_AGE")
        .expect("JWT_MAX_AGE is not set in .env file")
        .parse()
        .unwrap();

    let db_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");
    let db_connection = get_db_connection(db_url).await.unwrap();

    let middleware_stack = compose_middleware_stack();

    let app_state = Arc::new(AppState {
        db_connection: db_connection,
        jwt_secret,
        _jwt_expires_in: jwt_expires_in,
        jwt_max_age,
    });

    let app = Router::new()
        .layer(middleware_stack)
        .layer(
            ServiceBuilder::new()
                .layer(HandleErrorLayer::new(|_: BoxError| async {
                    StatusCode::REQUEST_TIMEOUT
                }))
                .layer(TimeoutLayer::new(Duration::from_secs(2))),
        )
        .nest("/api", get_api_router(&app_state))
        .route("/test", get(test))
        .fallback(fallback_handler);

    let socket_addr = SocketAddr::new(IpAddr::V4(ip), port);

    tracing::debug!("listening on {}", socket_addr);

    let listener = TcpListener::bind(&socket_addr).await.unwrap();
    axum::serve(listener, app.into_make_service())
        .await
        .unwrap();
}

async fn fallback_handler() -> (StatusCode, &'static str) {
    (StatusCode::NOT_FOUND, "Not Found")
}

async fn test() -> Result<&'static str, CustomError> {
    Ok("test")
}
