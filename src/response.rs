use core::fmt;
use std::{env, error::Error as StdError, num};

use axum::{
    http::StatusCode,
    response::{IntoResponse, Response},
};

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct HttpStatusCodeMessageError {
    pub status: StatusCode,
    pub message: String,
}

impl StdError for HttpStatusCodeMessageError {
    fn description(&self) -> &str {
        &self.message
    }
}

impl fmt::Display for HttpStatusCodeMessageError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.message)
    }
}

#[derive(thiserror::Error, Debug)]
pub enum CustomError {
    #[error("an error occurred: {0}")]
    StatusCodeMessageError(#[from] HttpStatusCodeMessageError),
    #[error("an error occurred: {0}")]
    AnyhowError(#[from] anyhow::Error),
    #[error("an error occurred: {0}")]
    VarError(#[from] env::VarError),
    #[error("an error occurred: {0}")]
    JsonWebTokenError(#[from] jsonwebtoken::errors::Error),
    #[error("a db error occurred: {0}")]
    DbError(#[from] sea_orm::DbErr),
    #[error("a parse error occurred: {0}")]
    ParseIntError(#[from] num::ParseIntError),
    #[error("a transaction error occurred: {0}")]
    TransactionError(#[from] sea_orm::TransactionError<sea_orm::DbErr>),
    #[error("a serde qs error occurred: {0}")]
    SerdeQSError(#[from] serde_qs::Error),
    #[error("invalid token error occurred")]
    InvalidToken,
    #[error("missing permission error occurred")]
    AuthMissingPermission,
    #[error("missing role error occurred")]
    AuthMissingRole,
}

impl CustomError {
    pub fn new(status_code: StatusCode, message: &str) -> Self {
        Self::StatusCodeMessageError(HttpStatusCodeMessageError {
            status: status_code,
            message: String::from(message),
        })
    }
}

impl IntoResponse for CustomError {
    fn into_response(self) -> Response {
        match &self {
            Self::StatusCodeMessageError(error) => {
                (error.status, error.message.clone()).into_response()
            }
            Self::AnyhowError(error) => {
                (StatusCode::INTERNAL_SERVER_ERROR, error.to_string()).into_response()
            }
            Self::VarError(error) => {
                (StatusCode::INTERNAL_SERVER_ERROR, error.to_string()).into_response()
            }
            Self::JsonWebTokenError(error) => {
                (StatusCode::BAD_REQUEST, error.to_string()).into_response()
            }
            Self::ParseIntError(error) => {
                (StatusCode::INTERNAL_SERVER_ERROR, error.to_string()).into_response()
            }
            Self::TransactionError(error) => {
                (StatusCode::INTERNAL_SERVER_ERROR, error.to_string()).into_response()
            }
            Self::SerdeQSError(error) => {
                (StatusCode::INTERNAL_SERVER_ERROR, error.to_string()).into_response()
            }
            Self::InvalidToken => (StatusCode::UNAUTHORIZED, "Invalid or missing token").into_response(),
            Self::AuthMissingPermission => {
                (StatusCode::UNAUTHORIZED, "Missing permission").into_response()
            }
            Self::AuthMissingRole => (StatusCode::UNAUTHORIZED, "Missing role").into_response(),
            Self::DbError(error) => match error {
                sea_orm::DbErr::RecordNotFound(message) => {
                    (StatusCode::NOT_FOUND, message.to_string()).into_response()
                }
                sea_orm::DbErr::AttrNotSet(message) => {
                    (StatusCode::BAD_REQUEST, message.to_string()).into_response()
                }
                sea_orm::DbErr::Type(message) => {
                    (StatusCode::BAD_REQUEST, message.to_string()).into_response()
                }
                sea_orm::DbErr::Json(message) => {
                    (StatusCode::BAD_REQUEST, message.to_string()).into_response()
                }
                sea_orm::DbErr::RecordNotInserted => {
                    (StatusCode::BAD_REQUEST, "Record not inserted").into_response()
                }
                sea_orm::DbErr::RecordNotUpdated => {
                    (StatusCode::BAD_REQUEST, "Record not updated").into_response()
                }
                sea_orm::DbErr::ConnectionAcquire(error) => {
                    (StatusCode::INTERNAL_SERVER_ERROR, error.to_string()).into_response()
                }
                sea_orm::DbErr::Conn(_error) => {
                    (StatusCode::INTERNAL_SERVER_ERROR, "Connection failed").into_response()
                }
                sea_orm::DbErr::Exec(_error) => {
                    (StatusCode::INTERNAL_SERVER_ERROR, "Execution failed").into_response()
                }
                sea_orm::DbErr::Query(_error) => {
                    (StatusCode::INTERNAL_SERVER_ERROR, "Query failed").into_response()
                }
                sea_orm::DbErr::ConvertFromU64(_) => (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    "Specified type cannot be converted from u64",
                )
                    .into_response(),
                sea_orm::DbErr::UnpackInsertId => (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    "Impossible to retrieve the last insert id after an insert statement",
                )
                    .into_response(),
                sea_orm::DbErr::UpdateGetPrimaryKey => (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    "Primary key not available for record update",
                )
                    .into_response(),
                sea_orm::DbErr::Custom(_message) => {
                    (StatusCode::INTERNAL_SERVER_ERROR, "Custom error occurred").into_response()
                }
                sea_orm::DbErr::Migration(_message) => (
                    StatusCode::INTERNAL_SERVER_ERROR,
                    "Migration error occurred",
                )
                    .into_response(),
                _ => (StatusCode::INTERNAL_SERVER_ERROR, "Unknown error occurred").into_response(),
            },
        }
    }
}

pub type CustomResult<T> = std::result::Result<T, CustomError>;
