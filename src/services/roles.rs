use crate::{
    models::{
        filter_pagination_sorting::{
            NameOrDescriptionSearchFilters, PaginateSortGroupBy, PaginatedItems,
        },
        roles::*,
    },
    response::CustomError,
};
use axum::http::StatusCode;
use entity::{permission, role, roles_permissions};
use sea_orm::{
    entity::*, DatabaseConnection, DatabaseTransaction, DbErr, PaginatorTrait, QueryFilter,
    QueryOrder, QuerySelect, TransactionTrait,
};
use sea_query::{extension::postgres::PgExpr, Expr, Func};

pub async fn get_paginated_roles(
    db_connection: &DatabaseConnection,
    filter: &NameOrDescriptionSearchFilters,
    paginate_sort_group_by: &PaginateSortGroupBy,
) -> Result<PaginatedItems<Role>, CustomError> {
    let mut query = role::Entity::find();
    if let Some(name_or_description) = filter.name_or_description.clone() {
        let name_or_description = format!("%{}%", name_or_description.trim());
        if !name_or_description.is_empty() {
            query = query.filter(
                Func::lower(Expr::col(role::Column::Name))
                    .ilike(&name_or_description)
                    .or(Func::lower(Expr::col(role::Column::Description))
                        .ilike(&name_or_description)),
            );
        }
    }
    if let Some(sort_by) = &paginate_sort_group_by.sort_by {
        for sort in sort_by {
            match sort.key.as_str() {
                "name" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(role::Column::Name)
                    } else {
                        query.order_by_asc(role::Column::Name)
                    };
                }
                "description" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(role::Column::Description)
                    } else {
                        query.order_by_asc(role::Column::Description)
                    };
                }
                _ => {}
            }
        }
    } else {
        query = query
            .order_by_asc(role::Column::Name)
            .order_by_asc(role::Column::Description);
    }
    let num_filtered_users = query.clone().count(db_connection).await?;

    if paginate_sort_group_by.items_per_page > 0 {
        query = query
            .offset(
                (paginate_sort_group_by.page - 1) * paginate_sort_group_by.items_per_page as u64,
            )
            .limit(paginate_sort_group_by.items_per_page as u64)
    }
    let paginated_roles = query.all(db_connection).await?;
    let mut paginated_roles_with_permissions = Vec::new();
    for role in paginated_roles.iter() {
        let permissions = role
            .find_related(permission::Entity)
            .order_by_asc(role::Column::Name)
            .order_by_asc(role::Column::Description)
            .all(db_connection)
            .await?;
        paginated_roles_with_permissions.push(Role::from(&(role.to_owned(), permissions)));
    }

    Ok(PaginatedItems {
        items: paginated_roles_with_permissions,
        total_items: num_filtered_users,
    })
}

pub async fn get_all_roles(db_connection: &DatabaseConnection) -> Result<Vec<Role>, CustomError> {
    let roles = role::Entity::find()
        .find_with_related(permission::Entity)
        .order_by_asc(role::Column::Name)
        .all(db_connection)
        .await?;
    let roles: Vec<Role> = roles.iter().map(|u| Role::from(u)).collect();
    Ok(roles)
}

pub async fn get_role_by_id(
    db_connection: &DatabaseConnection,
    id: i32,
) -> Result<UpdatedRole, CustomError> {
    let roles = role::Entity::find_by_id(id)
        .find_with_related(permission::Entity)
        .all(db_connection)
        .await?;
    let role = roles.first();
    match role {
        Some(role) => Ok(UpdatedRole::from(role)),
        None => Err(CustomError::new(StatusCode::NOT_FOUND, "Role not found")),
    }
}

async fn update_role_permissions(
    trx: &DatabaseTransaction,
    role_permissions: &Vec<permission::Model>,
    item: &UpdatedRole,
) -> Result<(), CustomError> {
    {
        let mut new_permissions = Vec::new();
        for permission_id in &item.permissions {
            if !role_permissions.iter().any(|p| p.id == *permission_id) {
                let role_permission = roles_permissions::ActiveModel {
                    role_id: Set(item.id),
                    permission_id: Set(*permission_id),
                };
                new_permissions.push(role_permission);
            }
        }
        if new_permissions.len() > 0 {
            let _res = roles_permissions::Entity::insert_many(new_permissions)
                .exec(trx)
                .await?;
        }
    }
    {
        let mut deletable_permissions = Vec::new();
        {
            for permission in role_permissions {
                if !item.permissions.contains(&permission.id) {
                    deletable_permissions.push(permission.id);
                }
            }
        }
        let num_deletable_permissions = deletable_permissions.len();
        if num_deletable_permissions > 0 {
            let res = roles_permissions::Entity::delete_many()
                .filter(
                    roles_permissions::Column::RoleId
                        .eq(item.id)
                        .and(roles_permissions::Column::PermissionId.is_in(deletable_permissions)),
                )
                .exec(trx)
                .await?;
            if res.rows_affected as usize != num_deletable_permissions {
                return Err(CustomError::DbError(DbErr::Custom(format!(
                    "Error deleting role permissions ({} != {})",
                    res.rows_affected, num_deletable_permissions
                ))));
            }
        }
    }

    Ok(())
}

pub async fn create_role(
    db_connection: &DatabaseConnection,
    item: &CreatedRole,
) -> Result<UpdatedRole, CustomError> {
    let trx = db_connection.begin().await?;

    let active_model = role::ActiveModel {
        name: Set(item.name.clone()),
        description: Set(item.description.clone()),
        ..Default::default()
    };
    let role = active_model.insert(&trx).await?;
    let item = UpdatedRole {
        id: role.id,
        name: item.name.clone(),
        description: item.description.clone(),
        permissions: item.permissions.clone(),
    };

    update_role_permissions(&trx, &Vec::new(), &item).await?;

    trx.commit().await?;

    let role_id = role.id;
    let roles = role::Entity::find_by_id(role_id)
        .find_with_related(permission::Entity)
        .all(db_connection)
        .await?;
    let role = roles.first();
    match role {
        Some(role) => Ok(UpdatedRole::from(role)),
        None => Err(CustomError::new(
            StatusCode::INTERNAL_SERVER_ERROR,
            format!("Can't retrieve role {} after creation", role_id).as_str(),
        )),
    }
}

pub async fn update_role(
    db_connection: &DatabaseConnection,
    id: i32,
    item: &UpdatedRole,
) -> Result<UpdatedRole, CustomError> {
    let roles = role::Entity::find_by_id(id)
        .find_with_related(permission::Entity)
        .all(db_connection)
        .await?;
    let role = roles.first();
    match role {
        Some((role, permissions)) => {
            let trx = db_connection.begin().await?;
            let mut active_model = role.clone().into_active_model();
            active_model.name = Set(item.name.clone());
            active_model.description = Set(item.description.clone());

            update_role_permissions(&trx, &permissions, item).await?;

            let active_model = active_model.save(&trx).await?;
            trx.commit().await?;

            let role_id = active_model.id.unwrap();
            let roles = role::Entity::find_by_id(role_id)
                .find_with_related(permission::Entity)
                .all(db_connection)
                .await?;
            let role = roles.first();
            match role {
                Some(role) => Ok(UpdatedRole::from(role)),
                None => Err(CustomError::new(
                    StatusCode::INTERNAL_SERVER_ERROR,
                    format!("Can't retrieve role {} after update", role_id).as_str(),
                )),
            }
        }
        None => Err(CustomError::new(StatusCode::NOT_FOUND, "User not found")),
    }
}

pub async fn delete_role(db_connection: &DatabaseConnection, id: i32) -> Result<u64, CustomError> {
    let role = role::Entity::find_by_id(id).one(db_connection).await?;
    match role {
        Some(role) => {
            let role: role::ActiveModel = role.into();
            let res = role.delete(db_connection).await?;
            Ok(res.rows_affected)
        }
        None => Err(CustomError::new(StatusCode::NOT_FOUND, "Role not found")),
    }
}
