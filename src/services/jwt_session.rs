use axum::{
    body::Body,
    extract::{FromRef, FromRequestParts, Request, State},
    http::{
        header::{self},
        request::Parts,
    },
    middleware::Next,
    response::IntoResponse,
    RequestPartsExt,
};
use axum_extra::{
    headers::{authorization::Bearer, Authorization},
    TypedHeader,
};
use chrono::{Duration, TimeDelta, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use sea_orm::DatabaseConnection;
use serde::{Deserialize, Serialize};
use std::sync::Arc;

use crate::{models::users::User, response::CustomError, state::AppState};

use super::users::get_user_by_id;

#[derive(Debug, Serialize, Deserialize)]
pub struct TokenClaims {
    pub sub: String,
    pub exp: usize,
    pub iat: usize,
}

pub fn generate_token(
    jwt_max_age: i64,
    jwt_secret: &[u8],
    user: &User,
) -> Result<String, CustomError> {
    let now = Utc::now();
    let iat = now.timestamp() as usize;
    let minutes = match Duration::try_minutes(jwt_max_age) {
        Some(mins) => mins,
        None => TimeDelta::zero(),
    };
    let exp = (now + minutes).timestamp() as usize;
    let claims = TokenClaims {
        sub: user.id.to_string(),
        exp,
        iat,
    };

    let token = encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(jwt_secret),
    )?;
    Ok(token)
}

pub fn extract_token(req: &Request<Body>) -> Result<&str, CustomError> {
    if let Some(auth_header) = req.headers().get(header::AUTHORIZATION) {
        if let Ok(auth_value) = auth_header.to_str() {
            if auth_value.starts_with("Bearer ") {
                return Ok(&auth_value[7..]);
            }
        }
    }

    Err(CustomError::InvalidToken)
}

pub async fn extract_user_info(
    db_connection: &DatabaseConnection,
    jwt_secret: &str,
    token: &str,
) -> Result<User, CustomError> {
    let claims = decode::<TokenClaims>(
        token,
        &DecodingKey::from_secret(jwt_secret.as_bytes()),
        &Validation::default(),
    )
    .map_err(|_| CustomError::InvalidToken)?
    .claims;

    let user_id: i32 = claims.sub.parse()?;
    let user = get_user_by_id(db_connection, user_id).await?;
    Ok(user)
}

pub async fn retrieve_session_user(
    db_connection: &DatabaseConnection,
    jwt_secret: &str,
    req: &mut Request<Body>,
) -> Result<User, CustomError> {
    let token = extract_token(&req)?;
    extract_user_info(db_connection, jwt_secret, token).await
}

pub async fn authorize_request(
    State(app_state): State<Arc<AppState>>,
    mut req: Request<Body>,
    next: Next,
) -> Result<impl IntoResponse, CustomError> {
    let user =
        retrieve_session_user(&app_state.db_connection, &app_state.jwt_secret, &mut req).await?;
    req.extensions_mut().insert(user);
    let response_body = next.run(req).await;
    Ok(response_body)
}

pub async fn authorize_request_with_permission(
    permission: &'static str,
    app_state: Arc<AppState>,
    mut req: Request<Body>,
    next: Next,
) -> Result<impl IntoResponse, CustomError> {
    let user =
        retrieve_session_user(&app_state.db_connection, &app_state.jwt_secret, &mut req).await?;
    if let Some(role) = &user.role {
        if !role.permissions.iter().any(|p| p.name == permission) {
            return Err(CustomError::AuthMissingPermission);
        }
    } else {
        return Err(CustomError::AuthMissingRole);
    }
    req.extensions_mut().insert(user);
    let response_body = next.run(req).await;
    Ok(response_body)
}

impl<S> FromRequestParts<S> for User
where
    Arc<AppState>: FromRef<S>,
    S: Send + Sync,
{
    type Rejection = CustomError;

    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let TypedHeader(Authorization(bearer)) = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await
            .map_err(|_| CustomError::InvalidToken)?;

        let state = Arc::from_ref(state);

        // Decode the user data
        let token_claims = decode::<TokenClaims>(
            bearer.token(),
            &DecodingKey::from_secret(state.jwt_secret.as_bytes()),
            &Validation::default(),
        )
        .map_err(|_| CustomError::InvalidToken)?
        .claims;

        let user_id: i32 = token_claims.sub.parse()?;
        let user = get_user_by_id(&state.db_connection, user_id).await?;

        Ok(user)
    }
}
