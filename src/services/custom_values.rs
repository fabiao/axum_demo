use crate::{
    models::{
        custom_values::*,
        filter_pagination_sorting::{PaginateSortGroupBy, PaginatedItems},
    },
    response::CustomError,
};
use axum::http::StatusCode;
use entity::custom_value;
use sea_orm::{entity::*, query::*, DatabaseConnection};
use serde_json::json;

pub async fn get_paginated_custom_values(
    db_connection: &DatabaseConnection,
    paginate_sort_group_by: &PaginateSortGroupBy,
) -> Result<PaginatedItems<CustomValue>, CustomError> {
    let mut query = custom_value::Entity::find();

    if let Some(sort_by) = &paginate_sort_group_by.sort_by {
        for sort in sort_by {
            match sort.key.as_str() {
                "code" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(custom_value::Column::Code)
                    } else {
                        query.order_by_asc(custom_value::Column::Code)
                    }
                }
                "description" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(custom_value::Column::Description)
                    } else {
                        query.order_by_asc(custom_value::Column::Description)
                    };
                }
                _ => {}
            }
        }
    } else {
        query = query
            .order_by_asc(custom_value::Column::Code)
            .order_by_asc(custom_value::Column::Description);
    }

    let num_filtered_custom_values = query.clone().count(db_connection).await?;

    if paginate_sort_group_by.items_per_page > 0 {
        query = query
            .offset(
                (paginate_sort_group_by.page - 1) * paginate_sort_group_by.items_per_page as u64,
            )
            .limit(paginate_sort_group_by.items_per_page as u64)
    }
    let paged_custom_values = query.all(db_connection).await?;
    let custom_values: Vec<CustomValue> = paged_custom_values
        .iter()
        .map(|cv| CustomValue::from(cv))
        .collect();

    Ok(PaginatedItems {
        items: custom_values,
        total_items: num_filtered_custom_values,
    })
}

pub async fn get_all_custom_values(
    db_connection: &DatabaseConnection,
) -> Result<Vec<BaseCustomValue>, CustomError> {
    let custom_values = custom_value::Entity::find()
        .select_only()
        .columns([custom_value::Column::Code, custom_value::Column::Description])
        .order_by_asc(custom_value::Column::Code)
        .into_model::<BaseCustomValue>()
        .all(db_connection)
        .await?;
    Ok(custom_values)
}

pub async fn get_custom_value_by_code(
    db_connection: &DatabaseConnection,
    code: String,
) -> Result<CustomValue, CustomError> {
    let custom_value = custom_value::Entity::find()
        .filter(custom_value::Column::Code.like(code))
        .one(db_connection)
        .await?;
    match custom_value {
        Some(custom_value) => Ok(CustomValue::from(&custom_value)),
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Custom value not found",
        )),
    }
}

pub async fn get_schema_by_code(
    db_connection: &DatabaseConnection,
    code: String,
) -> Result<JsonValue, CustomError> {
    let custom_value_schema = custom_value::Entity::find()
        .select_only()
        .column(custom_value::Column::Schema)
        .filter(custom_value::Column::Code.like(code))
        .into_model::<JsonValue>()
        .one(db_connection)
        .await?;
    match custom_value_schema {
        Some(schema) => Ok(schema.get("schema").unwrap().clone()),
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Custom value not found",
        )),
    }
}

pub async fn get_items_by_code(
    db_connection: &DatabaseConnection,
    code: String,
) -> Result<JsonValue, CustomError> {
    let custom_value_items = custom_value::Entity::find()
        .select_only()
        .column(custom_value::Column::Items)
        .filter(custom_value::Column::Code.like(code))
        .into_model::<JsonValue>()
        .one(db_connection)
        .await?;
    match custom_value_items {
        Some(items) => Ok(items.get("items").unwrap().clone()),
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Custom value not found",
        )),
    }
}

pub async fn create_custom_value(
    db_connection: &DatabaseConnection,
    item: CreatedCustomValue,
) -> Result<CustomValue, CustomError> {
    let empty_vector: Vec<()> = Vec::new();
    let custom_value = custom_value::ActiveModel {
        code: Set(item.code),
        description: Set(item.description),
        schema: Set(json!(empty_vector)),
        items: Set(json!(empty_vector)),
        is_active: Set(item.is_active),
        ..Default::default()
    };
    let custom_value = custom_value.insert(db_connection).await?;
    Ok(CustomValue::from(&custom_value))
}

pub async fn update_custom_value(
    db_connection: &DatabaseConnection,
    code: String,
    item: CustomValue,
) -> Result<CustomValue, CustomError> {
    let custom_value = custom_value::Entity::find()
        .filter(custom_value::Column::Code.like(code))
        .one(db_connection)
        .await?;
    match custom_value {
        Some(custom_value) => {
            let mut custom_value: custom_value::ActiveModel = custom_value.into();
            custom_value.code = Set(item.code);
            custom_value.description = Set(item.description);
            custom_value.is_active = Set(item.is_active);
            let custom_value = custom_value.update(db_connection).await?;
            Ok(CustomValue::from(&custom_value))
        }
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Custom value not found",
        )),
    }
}

pub async fn update_schema(
    db_connection: &DatabaseConnection,
    code: String,
    schema: JsonValue,
) -> Result<CustomValue, CustomError> {
    let custom_value = custom_value::Entity::find()
        .filter(custom_value::Column::Code.like(code))
        .one(db_connection)
        .await?;
    match custom_value {
        Some(custom_value) => {
            let mut custom_value: custom_value::ActiveModel = custom_value.into();
            custom_value.schema = Set(schema);
            let custom_value = custom_value.update(db_connection).await?;
            Ok(CustomValue::from(&custom_value))
        }
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Custom value not found",
        )),
    }
}

pub async fn update_items(
    db_connection: &DatabaseConnection,
    code: String,
    items: JsonValue,
) -> Result<CustomValue, CustomError> {
    let custom_value = custom_value::Entity::find()
    .filter(custom_value::Column::Code.like(code))
    .one(db_connection)
    .await?;
    match custom_value {
        Some(custom_value) => {
            let mut custom_value: custom_value::ActiveModel = custom_value.into();
            custom_value.items = Set(items);
            let custom_value = custom_value.update(db_connection).await?;
            Ok(CustomValue::from(&custom_value))
        }
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Custom value not found",
        )),
    }
}

pub async fn delete_custom_value(
    db_connection: &DatabaseConnection,
    code: String,
) -> Result<u64, CustomError> {
    let custom_value = custom_value::Entity::find()
        .filter(custom_value::Column::Code.like(code))
        .one(db_connection)
        .await?;
    match custom_value {
        Some(custom_value) => {
            let custom_value: custom_value::ActiveModel = custom_value.into();
            let res = custom_value.delete(db_connection).await?;
            Ok(res.rows_affected)
        }
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Custom value not found",
        )),
    }
}
