use crate::{
    models::{
        filter_pagination_sorting::{
            NameOrDescriptionSearchFilters, PaginateSortGroupBy, PaginatedItems,
        },
        permissions::*,
    },
    response::CustomError,
};
use axum::http::StatusCode;
use entity::permission;
use prelude::Expr;
use sea_orm::{entity::*, query::*, DatabaseConnection};
use sea_query::{extension::postgres::PgExpr, Func};

pub async fn get_paginated_permissions(
    db_connection: &DatabaseConnection,
    filter: &NameOrDescriptionSearchFilters,
    paginate_sort_group_by: &PaginateSortGroupBy,
) -> Result<PaginatedItems<Permission>, CustomError> {
    let mut query = permission::Entity::find();
    if let Some(name_or_description) = filter.name_or_description.clone() {
        let name_or_description = format!("%{}%", name_or_description.trim());
        if !name_or_description.is_empty() {
            query = query.filter(
                Func::lower(Expr::col(permission::Column::Name))
                    .ilike(&name_or_description)
                    .or(Func::lower(Expr::col(permission::Column::Description))
                        .ilike(&name_or_description)),
            );
        }
    }
    if let Some(sort_by) = &paginate_sort_group_by.sort_by {
        for sort in sort_by {
            match sort.key.as_str() {
                "name" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(permission::Column::Name)
                    } else {
                        query.order_by_asc(permission::Column::Name)
                    }
                }
                "description" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(permission::Column::Description)
                    } else {
                        query.order_by_asc(permission::Column::Description)
                    };
                }
                _ => {}
            }
        }
    } else {
        query = query
            .order_by_asc(permission::Column::Name)
            .order_by_asc(permission::Column::Description);
    }

    let num_filtered_permissions = query.clone().count(db_connection).await?;

    if paginate_sort_group_by.items_per_page > 0 {
        query = query
            .offset(
                (paginate_sort_group_by.page - 1) * paginate_sort_group_by.items_per_page as u64,
            )
            .limit(paginate_sort_group_by.items_per_page as u64)
    }
    let paged_permissions = query.all(db_connection).await?;
    let permissions: Vec<Permission> = paged_permissions
        .iter()
        .map(|u| Permission::from(u))
        .collect();

    Ok(PaginatedItems {
        items: permissions,
        total_items: num_filtered_permissions,
    })
}

pub async fn get_all_permissions(
    db_connection: &DatabaseConnection,
) -> Result<Vec<Permission>, CustomError> {
    let permissions = permission::Entity::find()
        .order_by_asc(permission::Column::Name)
        .all(db_connection)
        .await?;
    let permissions: Vec<Permission> = permissions.iter().map(|u| Permission::from(u)).collect();
    Ok(permissions)
}

pub async fn get_permission_by_id(
    db_connection: &DatabaseConnection,
    id: i32,
) -> Result<Permission, CustomError> {
    let permission = permission::Entity::find_by_id(id)
        .one(db_connection)
        .await?;
    match permission {
        Some(permission) => Ok(Permission::from(&permission)),
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Permission not found",
        )),
    }
}

pub async fn create_permission(
    db_connection: &DatabaseConnection,
    item: CreatedPermission,
) -> Result<Permission, CustomError> {
    let permission = permission::ActiveModel {
        name: Set(item.name),
        description: Set(item.description),
        ..Default::default()
    };
    let permission = permission.insert(db_connection).await?;
    Ok(Permission::from(&permission))
}

pub async fn update_permission(
    db_connection: &DatabaseConnection,
    id: i32,
    item: Permission,
) -> Result<Permission, CustomError> {
    let permission = permission::Entity::find_by_id(id)
        .one(db_connection)
        .await?;
    match permission {
        Some(permission) => {
            let mut permission: permission::ActiveModel = permission.into();
            permission.name = Set(item.name);
            if let Some(description) = item.description {
                permission.description = Set(Some(description));
            }
            let permission = permission.update(db_connection).await?;
            Ok(Permission::from(&permission))
        }
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Permission not found",
        )),
    }
}

pub async fn delete_permission(
    db_connection: &DatabaseConnection,
    id: i32,
) -> Result<u64, CustomError> {
    let permission = permission::Entity::find_by_id(id)
        .one(db_connection)
        .await?;
    match permission {
        Some(permission) => {
            let permission: permission::ActiveModel = permission.into();
            let res = permission.delete(db_connection).await?;
            Ok(res.rows_affected)
        }
        None => Err(CustomError::new(
            StatusCode::NOT_FOUND,
            "Permission not found",
        )),
    }
}
