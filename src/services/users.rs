use crate::{
    models::{
        filter_pagination_sorting::{PaginateSortGroupBy, PaginatedItems},
        users::{CreatedUser, ModifiedUser, User, UserSearchFilters},
    },
    response::CustomError,
};
use axum::http::StatusCode;
use chrono::Local;
use entity::{permission, role, user};
use sea_orm::{
    ActiveModelTrait, ColumnTrait, DatabaseConnection, EntityTrait, PaginatorTrait, QueryFilter,
    QueryOrder, QuerySelect, Set,
};
use sea_query::{extension::postgres::PgExpr, Expr, Func};

pub async fn get_all_users(db_connection: &DatabaseConnection) -> Result<Vec<User>, CustomError> {
    let all_users = user::Entity::find()
        .find_also_related(entity::role::Entity)
        .order_by_asc(user::Column::Email)
        .all(db_connection)
        .await?;
    let users = get_users_with_role_and_permissions(db_connection, &all_users).await?;
    Ok(users)
}

async fn get_user_with_role_and_permissions(
    db_connection: &DatabaseConnection,
    (user, role): &(user::Model, Option<role::Model>),
) -> Result<User, CustomError> {
    if let Some(role) = role {
        let role_permissions = permission::Entity::find()
            .left_join(role::Entity)
            .filter(role::Column::Id.eq(role.id))
            .order_by_asc(permission::Column::Name)
            .order_by_asc(permission::Column::Description)
            .all(db_connection)
            .await?;
        return Ok(User::from(&(
            user.to_owned(),
            (role.to_owned(), role_permissions),
        )));
    }

    Ok(User::from(&user.to_owned()))
}

async fn get_users_with_role_and_permissions(
    db_connection: &DatabaseConnection,
    paginated_users: &Vec<(user::Model, Option<role::Model>)>,
) -> Result<Vec<User>, CustomError> {
    let mut paginated_users_with_role_permissions = Vec::new();
    for user_role in paginated_users.iter() {
        let user = get_user_with_role_and_permissions(db_connection, user_role).await?;
        paginated_users_with_role_permissions.push(user);
    }

    Ok(paginated_users_with_role_permissions)
}

pub async fn get_paginated_users(
    db_connection: &DatabaseConnection,
    filter: &UserSearchFilters,
    paginate_sort_group_by: &PaginateSortGroupBy,
) -> Result<PaginatedItems<User>, CustomError> {
    let mut query = user::Entity::find();
    if let Some(name) = filter.name.clone() {
        let name = format!("%{}%", name.trim());
        if !name.is_empty() {
            query = query.filter(
                Func::lower(Expr::col(user::Column::FirstName))
                    .ilike(&name)
                    .or(Func::lower(Expr::col(user::Column::LastName)).ilike(&name)),
            );
        }
    }
    if let Some(email) = filter.email.clone() {
        let email = format!("%{}%", email.trim());
        if !email.is_empty() {
            query = query.filter(Func::lower(Expr::col(user::Column::Email)).ilike(&email));
        }
    }
    if let Some(from) = filter.from.clone() {
        if !from.trim().is_empty() {
            query = query.filter(user::Column::CreatedAt.gte(&from));
        }
    }
    if let Some(to) = filter.to.clone() {
        if !to.trim().is_empty() {
            query = query.filter(user::Column::CreatedAt.lte(&to));
        }
    }
    if let Some(sort_by) = &paginate_sort_group_by.sort_by {
        for sort in sort_by {
            match sort.key.as_str() {
                "first_name" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(user::Column::FirstName)
                    } else {
                        query.order_by_asc(user::Column::FirstName)
                    };
                }
                "last_name" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(user::Column::LastName)
                    } else {
                        query.order_by_asc(user::Column::LastName)
                    };
                }
                "email" => {
                    query = if sort.order == "desc" {
                        query.order_by_desc(user::Column::Email)
                    } else {
                        query.order_by_asc(user::Column::Email)
                    };
                }
                _ => {}
            }
        }
    } else {
        query = query
            .order_by_asc(user::Column::LastName)
            .order_by_asc(user::Column::FirstName);
    }

    let num_filtered_users = query.clone().count(db_connection).await?;

    if paginate_sort_group_by.items_per_page > 0 {
        query = query
            .offset(
                (paginate_sort_group_by.page - 1) * paginate_sort_group_by.items_per_page as u64,
            )
            .limit(paginate_sort_group_by.items_per_page as u64)
    }

    let paginated_users = query
        .find_also_related(role::Entity)
        .all(db_connection)
        .await?;
    let users = get_users_with_role_and_permissions(db_connection, &paginated_users).await?;

    Ok(PaginatedItems {
        items: users,
        total_items: num_filtered_users,
    })
}

pub async fn get_user_by_id(
    db_connection: &DatabaseConnection,
    id: i32,
) -> Result<User, CustomError> {
    let user_role = user::Entity::find_by_id(id)
        .find_also_related(role::Entity)
        .one(db_connection)
        .await?;
    match user_role {
        Some(user_role) => {
            let user = get_user_with_role_and_permissions(db_connection, &user_role).await?;
            Ok(user)
        }
        None => Err(CustomError::new(StatusCode::NOT_FOUND, "User not found")),
    }
}

pub async fn get_user_by_email(
    db_connection: &DatabaseConnection,
    email: &str,
) -> Result<User, CustomError> {
    let user_role = user::Entity::find()
        .filter(user::Column::Email.like(email))
        .find_also_related(role::Entity)
        .one(db_connection)
        .await?;
    match user_role {
        Some(user_role) => {
            let user = get_user_with_role_and_permissions(db_connection, &user_role).await?;
            Ok(user)
        }
        None => Err(CustomError::new(StatusCode::NOT_FOUND, "User not found")),
    }
}

pub async fn create_user(
    db_connection: &DatabaseConnection,
    item: CreatedUser,
) -> Result<User, CustomError> {
    let user = user::ActiveModel {
        first_name: Set(item.first_name),
        last_name: Set(item.last_name),
        email: Set(item.email),
        created_at: Set(Local::now().naive_utc()),
        role_id: Set(if let Some(role) = item.role {
            Some(role.id)
        } else {
            None
        }),
        ..Default::default()
    };
    let user = user.insert(db_connection).await?;
    Ok(User::from(&user))
}

pub async fn update_user(
    db_connection: &DatabaseConnection,
    id: i32,
    item: ModifiedUser,
) -> Result<User, CustomError> {
    let user = user::Entity::find_by_id(id).one(db_connection).await?;
    match user {
        Some(user) => {
            let mut user: user::ActiveModel = user.into();
            if let Some(first_name) = item.first_name {
                user.first_name = Set(first_name);
            }
            if let Some(last_name) = item.last_name {
                user.last_name = Set(last_name);
            }
            if let Some(email) = item.email {
                user.email = Set(email);
            }
            user.updated_at = Set(Some(Local::now().naive_utc()));
            user.role_id = Set(if let Some(role) = item.role {
                Some(role.id)
            } else {
                None
            });
            let user = user.update(db_connection).await?;
            Ok(User::from(&user))
        }
        None => Err(CustomError::new(StatusCode::NOT_FOUND, "User not found")),
    }
}

pub async fn delete_user(db_connection: &DatabaseConnection, id: i32) -> Result<u64, CustomError> {
    let user = user::Entity::find_by_id(id).one(db_connection).await?;
    match user {
        Some(user) => {
            let user: user::ActiveModel = user.into();
            let res = user.delete(db_connection).await?;
            Ok(res.rows_affected)
        }
        None => Err(CustomError::new(StatusCode::NOT_FOUND, "User not found")),
    }
}
