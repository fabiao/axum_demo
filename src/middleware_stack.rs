use tower::{
    layer::util::{Identity, Stack},
    ServiceBuilder,
};
use tower_http::{
    classify::{ServerErrorsAsFailures, SharedClassifier},
    compression::CompressionLayer,
    cors::{Any, CorsLayer},
    limit::RequestBodyLimitLayer,
    trace::{DefaultMakeSpan, DefaultOnResponse, TraceLayer},
};
use tracing::Level;

pub fn compose_middleware_stack() -> Stack<
    CorsLayer,
    Stack<
        CompressionLayer,
        Stack<
            TraceLayer<SharedClassifier<ServerErrorsAsFailures>>,
            Stack<RequestBodyLimitLayer, Identity>,
        >,
    >,
> {
    let middleware_stack = ServiceBuilder::new()
        .layer(tower_http::limit::RequestBodyLimitLayer::new(
            10 * 1024 * 1024,
        ))
        .layer(
            TraceLayer::new_for_http()
                .make_span_with(DefaultMakeSpan::new().level(Level::INFO))
                .on_response(DefaultOnResponse::new().level(Level::INFO)),
        )
        .layer(CompressionLayer::new())
        .layer(CorsLayer::new().allow_origin(Any))
        .into_inner();

    middleware_stack
}
