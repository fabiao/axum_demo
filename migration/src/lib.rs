pub use sea_orm_migration::prelude::*;

mod create_table;
mod m20240103_000001_create_permission_table;
mod m20240103_000002_create_user_table;
mod m20240103_000003_create_custom_value_table;
mod m20240103_000004_seed;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20240103_000001_create_permission_table::Migration),
            Box::new(m20240103_000002_create_user_table::Migration),
            Box::new(m20240103_000003_create_custom_value_table::Migration),
            Box::new(m20240103_000004_seed::Migration),
        ]
    }
}
