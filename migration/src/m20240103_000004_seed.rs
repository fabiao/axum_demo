use std::collections::HashMap;

use entity::{permission, role, roles_permissions, user};
use sea_orm_migration::{
    prelude::*,
    sea_orm::{entity::*, query::*, sqlx::types::chrono::Local, DatabaseTransaction},
};

#[derive(DeriveMigrationName)]
pub struct Migration;

async fn add_permissions(trx: &DatabaseTransaction) -> Result<HashMap<String, i32>, DbErr> {
    let mut indices = HashMap::new();

    // Permissions
    let permission = permission::ActiveModel {
        name: Set("PERMISSION_READ".to_owned()),
        description: Set(Some("Allow to read permissions".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    let permission = permission::ActiveModel {
        name: Set("PERMISSION_WRITE".to_owned()),
        description: Set(Some("Allow to write permissions".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    let permission = permission::ActiveModel {
        name: Set("PERMISSION_DELETE".to_owned()),
        description: Set(Some("Allow to delete permissions".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    // Roles
    let permission = permission::ActiveModel {
        name: Set("ROLE_READ".to_owned()),
        description: Set(Some("Allow to read roles".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    let permission = permission::ActiveModel {
        name: Set("ROLE_WRITE".to_owned()),
        description: Set(Some("Allow to write roles".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    let permission = permission::ActiveModel {
        name: Set("ROLE_DELETE".to_owned()),
        description: Set(Some("Allow to delete roles".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    // Users
    let permission = permission::ActiveModel {
        name: Set("USER_READ".to_owned()),
        description: Set(Some("Allow to read users".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    let permission = permission::ActiveModel {
        name: Set("USER_WRITE".to_owned()),
        description: Set(Some("Allow to write users".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    let permission = permission::ActiveModel {
        name: Set("USER_DELETE".to_owned()),
        description: Set(Some("Allow to delete users".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    // Custom values
    let permission = permission::ActiveModel {
        name: Set("CUSTOM_VALUE_READ".to_owned()),
        description: Set(Some("Allow to read custom values".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    let permission = permission::ActiveModel {
        name: Set("CUSTOM_VALUE_WRITE".to_owned()),
        description: Set(Some("Allow to write custom values".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    let permission = permission::ActiveModel {
        name: Set("CUSTOM_VALUE_DELETE".to_owned()),
        description: Set(Some("Allow to delete custom values".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(permission.name, permission.id);

    Ok(indices)
}

async fn add_roles(trx: &DatabaseTransaction) -> Result<HashMap<String, i32>, DbErr> {
    let mut indices = HashMap::new();

    let role = role::ActiveModel {
        name: Set("ADMIN".to_owned()),
        description: Set(Some("Allow to delete custom values".to_owned())),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    indices.insert(role.name, role.id);

    Ok(indices)
}

async fn add_users(trx: &DatabaseTransaction, role_id: i32) -> Result<(), DbErr> {
    let _user = user::ActiveModel {
        first_name: Set("John".to_owned()),
        last_name: Set("Doe".to_owned()),
        email: Set("john.doe@gmail.com".to_owned()),
        created_at: Set(Local::now().naive_utc()),
        role_id: Set(Some(role_id)),
        ..Default::default()
    }
    .insert(trx)
    .await?;

    Ok(())
}

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        let trx = db.begin().await?;

        let permission_indices = add_permissions(&trx).await?;
        let role_indices = add_roles(&trx).await?;

        for (role_name, role_id) in &role_indices {
            if role_name == "ADMIN" {
                for (_permission_name, permission_id) in &permission_indices {
                    let _role_permission = roles_permissions::ActiveModel {
                        role_id: Set(role_id.to_owned()),
                        permission_id: Set(permission_id.to_owned()),
                    }
                    .insert(&trx)
                    .await?;
                }

                add_users(&trx, role_id.to_owned()).await?;
            }
        }

        trx.commit().await?;

        Ok(())
    }

    async fn down(&self, _manager: &SchemaManager) -> Result<(), DbErr> {
        Ok(())
    }
}
