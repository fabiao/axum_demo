import { createApp } from "vue";
import ElementPlus from "element-plus";
import App from "~/App.vue";
import "element-plus/dist/index.css";
import "~/styles/index.scss";
import it from "element-plus/es/locale/lang/it";
import { setupStore } from "~/store";
import router from '~/router';

const setupAll = async () => {
  const app = createApp(App)
  .use(ElementPlus, {
    locale: it,
  })
  .use(router);

  setupStore(app);

  app.mount("#app");
};

setupAll();
