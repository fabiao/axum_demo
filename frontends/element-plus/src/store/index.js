import { createPinia } from "pinia";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";
import { useAppStore } from "~/store/modules/app";
import { useUserStore } from "~/store/modules/user";

const store = createPinia();

store.use(piniaPluginPersistedstate);

const setupStore = (app) => {
  // add modules
  useAppStore(store);
  useUserStore(store);
  
  // bind to app element
  app.use(store);
};

export { setupStore, store };