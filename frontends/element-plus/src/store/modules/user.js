import { defineStore } from "pinia";

const userState = {
  state: () => {
    return {
      userInfo: undefined,
      tokenKey: "Authorization",
      token: "",
      roleRouters: undefined,
      rememberMe: true,
      loginInfo: undefined,
    };
  },
  actions: {
    async logoutConfirm() {
        const { t } = useI18n();
        try {  
            await ElMessageBox.confirm(t("common.loginOutMessage"),
                t("common.reminder"), {
                    confirmButtonText: t("common.ok"),
                    cancelButtonText: t("common.cancel"),
                    type: "warning",
                });
            const res = await logoutApi();
            if (res) {
                this.reset();
            }
        } catch(e) {

        }
    },
  },
  persist: true
};

export const useUserStore = defineStore('user', {
    state: () => userState
})