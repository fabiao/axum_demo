import { defineStore } from "pinia";

const appState = {
  state: () => {
    return {
      sizeMap: ["default", "large", "small"],
      mobile: false,
      title: import.meta.env.VITE_APP_TITLE,
      pageLoading: false,
      breadcrumb: true,
      breadcrumbIcon: true,
      collapse: false,
      uniqueOpened: false,
      hamburger: true,
      screenfull: true,
      size: true,
      locale: true,
      tagsView: true,
      tagsViewIcon: true,
      logo: true,
      fixedHeader: true,
      footer: true,
      greyMode: false,
      fixedMenu: false,
      layout: "classic",
      isDark: false,
      currentSize: "default",
      theme: {
        elColorPrimary: "#409eff",
        leftMenuBorderColor: "inherit",
        leftMenuBgColor: "#001529",
        leftMenuBgLightColor: "#0f2438",
        leftMenuBgActiveColor: "var(--el-color-primary)",
        leftMenuCollapseBgActiveColor: "var(--el-color-primary)",
        leftMenuTextColor: "#bfcbd9",
        leftMenuTextActiveColor: "#fff",
        logoTitleTextColor: "#fff",
        logoBorderColor: "inherit",
        topHeaderBgColor: "#fff",
        topHeaderTextColor: "inherit",
        topHeaderHoverColor: "#f6f6f6",
        topToolBorderColor: "#eee",
      },
    };
  },
  actions: {
    toggleDarkLight() {
      this.isDark = isDark;
      if (this.isDark) {
        document.documentElement.classList.add("dark");
        document.documentElement.classList.remove("light");
      } else {
        document.documentElement.classList.add("light");
        document.documentElement.classList.remove("dark");
      }
      if (this.theme.elColorPrimary) {
        const elColorPrimary = this.theme.elColorPrimary;
        const color = this.isDark ? "#000000" : "#ffffff";
        const lightList = [3, 5, 7, 8, 9];
        lightList.forEach((v) => {
          setCssVar(
            `--el-color-primary-light-${v}`,
            mix(color, elColorPrimary, v / 10)
          );
        });
        setCssVar(`--el-color-primary-dark-2`, mix(color, elColorPrimary, 0.2));
      }
    },
  },
  persist: true
};

export const useAppStore = defineStore('app', {
    state: () => appState
})
