import { createWebHistory, createRouter } from "vue-router";

const routes = [
  {
    name: "Dashboard",
    path: "/",
    component: () => import("~/pages/Dashboard.vue"),
    hidden: false,
  },
  {
    name: "Login",
    path: "/login",
    component: () => import("~/pages/Login.vue"),
    hidden: false,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

export default router;
