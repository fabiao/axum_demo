import moment from 'moment';
import { formatDefaultDate } from './datetime';

const isEmail = (v: string) => {
  const er = /.+@.+\..+/;
  return er.test(v);
};

const isFloat = (v: string) => {
  const er = /^[-+]?[0-9]*\.?[0-9]+$/;
  return er.test(v);
};

const isInt = (v: string) => {
  const er = /^[-+]?[0-9]+$/;
  return er.test(v);
};

const isHex = (v: string) => {
  const er = /^[0-9a-fA-F\d\s]+$/;
  return er.test(v);
};

const isTime = (v: string) => {
  const er = /^([0-1][0-9]|[2][0-3]):([0-5][0-9])$/;
  return er.test(v);
};

const daysBetween = (date1: any, date2: any) => {
  const d1 = formatDefaultDate(date1);
  const d2 = moment(date2, 'YYYY-MM-DD');
  const days = Math.abs(d1.diff(d2, 'days')) + 1;
  return days;
};

const dateIsSameOrAfter = (date1: any, date2: any) => {
  return moment(date1, 'DD/MM/YYYY').isSameOrAfter(date2, 'day');
};

const dateIsSameOrBefore = (date1: any, date2: any) => {
  return moment(date1, 'DD/MM/YYYY').isSameOrBefore(date2, 'day');
};

const hasOnlyOne = (v: any, targets: Array<any>, field: string | number) => {
  const filteredItems = targets.filter((t: any) => t[field] === v);
  const numItems = filteredItems ? filteredItems.length : 0;
  return numItems <= 1;
};

const t = (message: string, params?: any) => {
  return message + params;
};

export const checkRequired = () => (v: string | any[] | null) =>
  (v instanceof Array && v.length > 0) || v != null || t('validation.fieldRequired');
export const checkAtLeastOneSelected = (targets: Array<any>, field: string) => () =>
  targets.some(target => target[field]) || t('validation.fieldRequired');
export const checkRequiredIfTargetIsNull = (target: any) => (v: any) => (target == null && v != null) || t('validation.fieldRequired');
export const checkEqualsTo = (target: any) => (v: any) => v == null || v === target || t('validation.fieldNotEqualsTo');
export const checkNotEqualsTo = (targets: Array<any>, field: string) => (v: any) =>
  v == null || hasOnlyOne(v, targets, field) || 'Valore già specificato altrove';
export const checkMinLength = (length: number) => (v: string) =>
  v == null || v.length >= length || t('validation.fieldWithMinChars', [length]);
export const checkMaxLength = (length: number) => (v: string) =>
  v == null || v.length <= length || t('validation.fieldWithMaxChars', [length]);
export const checkLengthIsOdd = (v: string) => v == null || v.length % 2 !== 0 || 'Necessario digitare un numero dispari di caratteri';
export const checkLengthIsEven = (v: string) => v == null || v.length % 2 === 0 || 'Necessario digitare un numero pari di caratteri';
export const checkMaxFileSize = (mbSize: number) => (v: File) =>
  v == null || v.size <= mbSize * 1000000 || v.size == null || t('validation.fieldWithMaxSize', [mbSize]);
export const checkEmail = () => (v: any) => v == null || isEmail(v) || t('validation.fieldEmailInvalid');
export const checkNum = () => (v: any) => v == null || isFloat(v.toString()) || t('validation.fieldMustBeNumeric');
export const checkInt = () => (v: any) => v == null || isInt(v.toString()) || t('validation.fieldMustBeInteger');
export const checkHex = () => (v: any) => v == null || isHex(v.toString()) || t('validation.fieldMustBeHex');
export const checkNumMin = (refValue: number) => (v: any) =>
  v == null || (!isNaN(v) && v >= refValue) || t('validation.fieldMustBeGreaterOrEqualTo', [refValue]);
export const checkNumMax = (refValue: number) => (v: any) =>
  v == null || (!isNaN(v) && v <= refValue) || t('validation.fieldMustBeLessOrEqualTo', [refValue]);
export const checkTime = () => (v: any) => v == null || isTime(v) || t('validation.fieldMustBeTime');
export const checkDateSameOrBefore = (target: any) => (v: any) =>
  !v || !target || dateIsSameOrBefore(v, target) || t('validation.dateIsSameOrBefore', [moment(target, 'YYYY-MM-DD').format('DD/MM/YYYY')]);
export const checkDateSameOrAfter = (target: any) => (v: any) =>
  !v || !target || dateIsSameOrAfter(v, target) || t('validation.dateIsSameOrAfter', [moment(target, 'YYYY-MM-DD').format('DD/MM/YYYY')]);
export const checkMinDaysBetween = (target: any, delta: number) => (v: any) =>
  !v ||
  !target ||
  daysBetween(v, target) >= delta ||
  t('validation.minDaysBetweenDates', [moment(target, 'YYYY-MM-DD').format('DD/MM/YYYY'), delta]);
export const checkMaxDaysBetween = (target: any, delta: number) => (v: any) =>
  !v ||
  !target ||
  daysBetween(v, target) <= delta ||
  t('validation.maxDaysBetweenDates', [moment(target, 'YYYY-MM-DD').format('DD/MM/YYYY'), delta]);
