import moment from 'moment';

export const formatDatetime = (datetime: unknown, datetimeFormat: string) => {
  return moment(datetime as moment.MomentInput, datetimeFormat, undefined).toString();
};

export const formatDefaultDatetime = (dateTime: unknown) => {
  return formatDatetime(dateTime, 'DD/MM/YYYY HH:mm');
};

export const formatDefaultDate = (date: unknown) => {
  return formatDatetime(date, 'DD/MM/YYYY');
};

export const formatISODate = (date: unknown) => {
  return formatDatetime(date, 'YYYY-MM-DD');
};

export const formatDefaultTime = (time: unknown) => {
  return formatDatetime(time, 'HH:mm');
};

export const formatDateFromTo = (dateTime: unknown, input: string, output: string | undefined) => {
  if (dateTime == null)
      return undefined;

  return moment(dateTime, input).format(output);
}
