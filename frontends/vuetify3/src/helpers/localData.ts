import type { UserInfo } from '../models/user';

export const saveToken = (token: string) => {
  localStorage.setItem('token', token);
};

export const removeToken = () => {
  localStorage.removeItem('token');
};

export const getToken = (): string | null => {
  return localStorage.getItem('token');
};

export const saveUser = (user: UserInfo) => {
  localStorage.setItem('user', JSON.stringify(user));
};

export const removeUser = () => {
  localStorage.removeItem('user');
};

export const getUser = (): UserInfo | null =>  {
  const userString = localStorage.getItem('user');
  return userString != null && userString != '' ? (JSON.parse(userString) as UserInfo) : null;
};

export const produceRedirectionPath = (path: string) => {
  localStorage.setItem('redirectTo', path);
  //alert('saved path: ' + path)
};

export const consumeRedirectionPath = (): string | null => {
  let path = localStorage.getItem('redirectTo');
  if (!path || path.indexOf('login') >= 0) {
    path = '/';
  }
  localStorage.removeItem('redirectTo');
  return path;
};
