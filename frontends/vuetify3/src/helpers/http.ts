import axios, { AxiosError, HttpStatusCode, type AxiosResponse } from 'axios';
import { getToken } from './localData';
import { useAuthStore } from '@/stores/auth';

const BASE_V1_API_PATH = import.meta.env.BASE_URL + 'api/';

const handleSuccess = (response: AxiosResponse) => {
  return response;
};

const handlerError = async (error: AxiosError) => {
  let message = "Unknown error occurred";
  if (error) {
    if (error.status === HttpStatusCode.Unauthorized || error.status === HttpStatusCode.Forbidden) {
      const auth = useAuthStore();
      auth.logoutConfirmed();
      window.location.reload();
    }

    if (error.response) {
      if (error.response.data) {
        message = error.response.data as string;
      } else if (error.response.statusText) {
        message = error.response.statusText;
      } else {
        message = JSON.stringify(error.response);
      }
    } else {
      message = JSON.stringify(error);
    }
  }

  return Promise.reject(message);
};

const http = axios.create({
  baseURL: BASE_V1_API_PATH,
  headers: {
    'Content-Type': 'application/json',
  },
});
http.interceptors.response.use(handleSuccess, handlerError);

const httpAuth = axios.create({
  baseURL: BASE_V1_API_PATH,
  headers: {
    'Content-Type': 'application/json',
  },
});

httpAuth.interceptors.request.use(config => {
  if (!config.headers.Authorization) {
    const token = getToken();
    if (token) {
      config.headers.Authorization = 'Bearer ' + getToken();
    }
  }

  return config;
});

httpAuth.interceptors.response.use(handleSuccess, handlerError);

export { http, httpAuth };
