import { defineStore } from 'pinia';

export const useNotificationsStore = defineStore('notify', {
  state: () => ({
    type: '',
    text: '',
    modal: false,
    show: false,
    timeout: 5000,
  }),
  actions: {
    clear() {
      this.type = '';
      this.text = '';
      this.modal = false;
      this.show = false;
    },
    info(message: string | unknown) {
      this.type = 'info';
      this.text = '' + message;
      this.show = true;
    },
    warning(message: string | unknown) {
      this.type = 'warning';
      this.text = '' + message;
      this.show = true;
    },
    success(message: string | unknown) {
      this.type = 'success';
      this.text = '' + message;
      this.show = true;
    },
    error(message: string | unknown) {
      this.type = 'error';
      this.text = '' + message;
      this.show = true;
    },
  },
});
