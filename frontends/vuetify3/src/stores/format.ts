import { defineStore } from "pinia";

export const useFormatStore = defineStore('format', {
  actions: {
    toBase64(text) {
        return Buffer.from(text).toString('base64');
    }
  }
});