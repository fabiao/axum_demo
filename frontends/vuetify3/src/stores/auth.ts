import { defineStore } from 'pinia';
import { useNotificationsStore } from './notifications';
import { type LoginInfo, type UserInfo } from '../models/user';
import { getUser, getToken, saveUser, saveToken, removeUser, removeToken } from '../helpers/localData';
import { http, httpAuth } from '../helpers/http';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    user: getUser(),
    token: getToken(),
    showPassword: false,
  }),
  actions: {
    isLoggedIn() {
      return this.user && this.token;
    },
    can(permission: string) {
      return this.user && this.user.role?.permissions.some(r => r.name == permission);
    },
    updateUser(user: UserInfo) {
      saveUser(user);
      this.user = user;
    },
    updateToken(token: string) {
      saveToken(token);
      this.token = token;
    },
    loginConfirmed(userInfo: UserInfo) {
      this.updateUser(userInfo);
    },
    logoutConfirmed() {
      removeToken();
      removeUser();
      this.user = null;
      this.token = null;
    },
    async login(loginInfo: LoginInfo) {
      try {
        {
          const { data } = await http.post('login', loginInfo);
          if (data) {
            const token = data;
            this.updateToken(token);
          } else {
            throw "Empty user session token retrieved";
          }
        }

        {
          const { data } = await httpAuth.get('users/profile');
          if (data) {
            this.loginConfirmed(data as UserInfo);
            return true;
          } else {
            throw "Empty user profile retrieved";
          }
        }
      } catch (e) {
        console.error(e)
        this.logoutConfirmed();
        const notify = useNotificationsStore();
        notify.error(e);
      }

      return false;
    },
    async refreshUser() {
      try {
        const { data } = await httpAuth.get('users/refresh');
        if (data) {
          this.updateToken(data.token);
          this.loginConfirmed(data.user);
          return data;
        }
      } catch (e) {
        this.logoutConfirmed();
        const notify = useNotificationsStore();
        notify.error(e);
      }

      return null;
    },
    async logout() {
      try {
        await httpAuth.post('logout');
      } catch (e) {
        const notify = useNotificationsStore();
        notify.error(e);
      }

      this.logoutConfirmed();
    },
    async requestPasswordReset(email: string) {
      const notify = useNotificationsStore();
      try {
        await http.post('request-password-reset', { email: email });
        notify.success('Email inviata correttamente');
      } catch (e) {
        notify.error(e);
      }
    },
    async resetPassword(data: object) {
      const notify = useNotificationsStore();
      try {
        await http.patch('reset-password', data);
        notify.success('Password resettata correttamente');
      } catch (e) {
        notify.error(e);
      }
    },
  },
});
