import type { RoleInfo } from "./role";

export interface UserInfo {
  id: number | null,
  first_name: string | null,
  last_name: string | null,
  email: string | null,
  created_at: string | null,
  updated_at: string | null,
  role: RoleInfo | null,
}


export interface LoginInfo {
  email: string | null,
  password: string | null,
}
