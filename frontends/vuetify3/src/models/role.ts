import type { PermissionInfo } from "./permission";

export interface RoleInfo {
  id: number | null,
  name: string | null,
  description: string | null,
  permissions: Array<PermissionInfo>,
}
