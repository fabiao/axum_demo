export interface TableHeader {
  title: string | null;
  align?: 'start' | 'end' | 'center';
  key: string | null;
  sortable?: boolean;
  width?: number;
}

export interface TableOptions {
  page: number,
  itemsPerPage: number,
}
