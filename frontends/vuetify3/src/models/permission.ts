export interface PermissionInfo {
  id: number | null,
  name: string | null,
  description: string | null,
}
