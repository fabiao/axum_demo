export interface SchemaValue {
    name: string,
    type: string | null,
}

export interface NewSchemaValue {
    name: string | null,
    type: string | null,
}

export interface ItemValue {
    code: string,
    description: string | null,
    isActive: boolean
}

export interface CustomValue {
    id: number | null,
    code: string | null,
    description: string | null,
    schema: Array<SchemaValue>,
    items: Array<unknown>,
    isActive: boolean
}

export interface LabelValues {
    labels: Array<string>,
    values: Array<number>
}

export enum ValueTypes {
    Text = 'text',
    LargeText = 'large_text',
    Number = 'number',
    Bool = 'bool'
}