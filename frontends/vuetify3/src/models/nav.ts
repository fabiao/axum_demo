export interface MenuItem {
  heading?: string,
  icon?: string,
  to?: string,
  text: string,
  children?: Array<MenuItem>,
}
