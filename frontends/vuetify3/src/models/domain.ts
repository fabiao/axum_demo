export interface DomainInfo {
  id: number | null,
  name: string | null,
  url: string | null,
}
